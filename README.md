# Collection of docker images

Note: this is work in progress.



# List of images:

- [rafalskolasinski/machine-learning](https://hub.docker.com/r/rafalskolasinski/machine-learning)


# Where to obtain images

Images in this repository are build in Gitlab CI and pushed to hub.docker.com.
Follow relevant link above to obtain the image.
