import subprocess
import yaml
import sys


def get_outdated_packages():
    p = subprocess.Popen(
        ["pip", "list", '--outdated'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    lines = p.communicate()[0].decode().rstrip("\n").split('\n')
    return [line.split() for line in lines[2:]]


def read_packages_of_interest(fname):
    with open(fname, 'r') as stream:
        try:
            env_spec = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


    packages_of_interest = [
        e.split('=')[0].split('::')[1]
        for e in list(env_spec['dependencies'])
    ]

    return packages_of_interest


def main(fname):
    packages_of_interest = read_packages_of_interest(fname)
    outdated = get_outdated_packages()

    for package, version, latest, _ in outdated:
        if package in packages_of_interest:
            print('Available update {}: {} -> {}'.format(package, version, latest))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('Must provide (one) conda environment file.')
    main(sys.argv[1])
